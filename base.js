$(document).ready(function () {
    var Student = {
        viewStudent : function(){
            let listData = JSON.parse(localStorage.getItem('qlsv'));
            let htmlInsert = "";
            // Lặp và hiển thị sinh viên
            if( listData ){
                for(let i = 0; i < listData.length; i++){
                    if(listData[i]){
                        htmlInsert += "<tr>"
                                        +"<td>"+ i +"</td>"
                                        +"<td>"+listData[i].id +"</td>"
                                        +"<td>"+listData[i].name +"</td>"
                                        +"<td>"+listData[i].address +"</td>"
                                        +"<td>"
                                            +"<button type=\"button\" class=\"btn mr-2 edit btn-warning\" data-id=\""+listData[i].id+"\">Edit</button>"
                                            +"<button type=\"button\" class=\"btn del btn-danger\" data-id=\""+listData[i].id+"\">Delete</button>"
                                        +"</td>"
                                    +"</tr>";
                    }
                    $("#list-sv tbody").html(htmlInsert);
                }
            }else{
                console.log('No data!');
            }
        },
        addStudent : function(id, name, address){
            // Tạo thông tin sinh viên
            var item = {
                id : id,
                name : name,
                address : address
            };
            var data = JSON.parse(localStorage.getItem('qlsv'));
            if(!data){
                data = [];
            }
            //Thêm sinh viên
            let buttonType = $('.btn-submit').attr('type');
            if(buttonType === "add"){
                data.push(item);
            }
            if (typeof(Storage) !== 'undefined') {
                //Nếu có hỗ trợ
                //Thực hiện thao tác với Storage
                localStorage.setItem('qlsv', JSON.stringify(data));
            } else {
                //Nếu không hỗ trợ
                alert('Trình duyệt của bạn không hỗ trợ Storage');
            }
        },
        removeStudent : function(id){
            // Lặp qua sinh viên để tìm kiếm và xóa
            let data = JSON.parse(localStorage.getItem('qlsv'));
            if( data ){
                for(let i = 0; i < data.length; i++){
                    console.log(data);
                    if (data[i].id === id) { // nếu là sinh viên cần xóa
                        data.splice(i, 1); // thì xóa
                        console.log(data);
                    }
                }
                localStorage.setItem('qlsv', JSON.stringify(data));
            }
        },
        editStudent : function(idEdit, id, name, address){
            console.log(id)
            let item = {
                id : id,
                name : name,
                address : address
            };
            
            let buttonType = $('.btn-submit').attr('type');
            // Tìm sinh viên cần edit
            let data = JSON.parse(localStorage.getItem('qlsv'));
            if( data ){
                for(let i = 0; i < data.length; i++){
                    if (data[i].id === idEdit) { // nếu là sinh viên cần edit
                        if(buttonType === "edit"){
                            data.splice(i, 1, item);
                        }
                    }
                }
                localStorage.setItem('qlsv', JSON.stringify(data));
            }
        }
    };
    Student.viewStudent();
    $('.btn-submit').click(function (e) { 
        e.preventDefault();
        let maSv =  $('#msv').val();
        let nameSV =  $('#name').val();
        let addressSv =  $('#address').val();
        let buttonType = $('.btn-submit').attr('type');
        if(buttonType === "add"){
            Student.addStudent(maSv, nameSV, addressSv);
        }
        Student.viewStudent();
        $('#add').toggleClass('d-none');
        // $('#form-add').find("input[type=text], textarea").val("");
        window.location.reload();
    });
    // Su kien xoa sinh vien
    $('.del').click(function (e) { 
        e.preventDefault();
        let id = $(this).attr('data-id');
        Student.removeStudent(id);
        Student.viewStudent();
        window.location.reload();
    });
    // Su kien sửa sinh vien
    $('.edit').click(function (e) { 
        e.preventDefault();
        // Doi trang thai submit
        $('.btn-submit').attr('type','edit');
        let idEdit = $(this).attr('data-id');
        $('#add').removeClass('d-none');
        let data = JSON.parse(localStorage.getItem('qlsv'));
        if( data ){
            for(let i = 0; i < data.length; i++){
                if (data[i].id === idEdit) { // nếu là sinh viên cần sửa fill data vào form
                    $('#msv').val(data[i].id);
                    $('#name').val(data[i].name);
                    $('#address').val(data[i].address);
                }
            }
        }
        
        $('.btn-submit').click(function (e) { 
            e.preventDefault();
            let maSv =  $('#msv').val();
            let nameSV =  $('#name').val();
            let addressSv =  $('#address').val();
            console.log(maSv)
            let buttonType = $('.btn-submit').attr('type');
            if(buttonType === "edit"){
                Student.editStudent(idEdit, maSv, nameSV, addressSv);
            }
        });
    });
    $('.btn-add-new').click(function (e) { 
        e.preventDefault();
        $('#add').toggleClass('d-none');
        $('.btn-submit').attr('type','add');
    });
    $('.btn-close').click(function (e) { 
        e.preventDefault();
        $('#add').toggleClass('d-none');
    });
});